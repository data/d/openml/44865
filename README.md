# OpenML dataset: concrete_compressive_strength

https://www.openml.org/d/44865

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

Concrete is the most important material in civil engineering. The concrete compressive strength is a highly nonlinear function of age and ingredients. Each instance represents a description, different features of concrete instance, including its compressive strength. The latter can be predicted using the other features about the concrete.

**Attribute Description**

1. *cement* - amount in kg in a m3 mixture
2. *blast_furnace_slag* - amount in kg in a m3 mixture
3. *fly_ash* - amount in kg in a m3 mixture
4. *water* - amount in kg in a m3 mixture
5. *superplasticizer* - amount in kg in a m3 mixture
6. *coarse_aggregate* - amount in kg in a m3 mixture
7. *fine_aggregate* - amount in kg in a m3 mixture
8. *age* - age in days (1 - 365)
9. *strength* - in MPa, target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44865) of an [OpenML dataset](https://www.openml.org/d/44865). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44865/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44865/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44865/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

